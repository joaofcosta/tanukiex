defmodule Tanukiex.BuildTriggerTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock
  import Tanukiex.BuildTrigger
  require IEx

  doctest Tanukiex.BuildTrigger

  @credentials Tanukiex.credentials("https://gitlab.com/api/v3/", "FqAkMavQAygVRi-fUsNv")

  test "list build triggers by project" do
    use_cassette "build_trigger#list_by_project" do
      response = @credentials |> list_by_project(1130437)
      assert response.status_code == 200
    end
  end

  test "create a build trigger for a project" do
    use_cassette "build_trigger#create" do
      response = @credentials |> create(1130437)
      assert response.status_code == 201
      assert response.body.token == "3286a75ef5a3a5e3016f7f3410d163"
    end
  end

  test "get details of project's build trigger" do
    use_cassette "build_trigger#details" do
      response = @credentials |> details(1130437, "3286a75ef5a3a5e3016f7f3410d163")
      assert response.status_code == 200
    end
  end

  test "remove a project's build trigger" do
    use_cassette "build_trigger#remove" do
      response = @credentials |> remove(1130437, "3286a75ef5a3a5e3016f7f3410d163")
      assert response.status_code == 200
    end
  end
end