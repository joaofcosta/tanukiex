defmodule Tanukiex.Serializer do
  @moduledoc """
    Tanukiex.Serializer module is response for
    encoding and parsing data from and to an url safe format
  """

  def encode(data), do: Poison.encode!(data)

  def decode(data), do: Poison.Parser.parse!(data, keys: :atoms)
end
