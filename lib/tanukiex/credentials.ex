defmodule Tanukiex.Credentials do
  @moduledoc """
    Tanukiex.Credentials module is responsible for encapsulating
    Gitlab credentials used for request authentication.
  """

  @typedoc """
  Credentials struct is used to build the Credentials map using 
  the `endpoint` and `private_token` fields.  
  """
  @type t :: %__MODULE__{endpoint: String.t, private_token: String.t}

  @doc """
  Build a credentials struct.
    * `endpoint` - defaults to http://www.gitlab.com/api/v3/
    * `private_token` - user private token, more info at 

    ```
    %Tanukiex.Credentials{endpoint: "http://www.gitlab.com/api/v3/", private_token: "bb8r2d2c3p0"}
    ```
  """

  @gitlab_endpoint "https://gitlab.com/api/v3/"
  defstruct endpoint: @gitlab_endpoint, private_token: nil
end
